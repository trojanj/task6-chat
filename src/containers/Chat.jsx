import React from 'react';
import { ChatHeader } from '../components/ChatHeader/ChatHeader';
import { MessageList } from '../components/MessageList/MessageList';
import { MessageInput } from '../components/MessageInput/MessageInput';
import Loader from '../components/Loader/Loader';
import { v4 as uuidv4 } from 'uuid';

import classes from './Chat.module.css';

export class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isLoading: false,
      textareaValue: '',
      user: {
        id: '9a233231-84c0-12e8-8g0n-8a1e646a4me7',
        name: 'Arnold',
        avatar: process.env.PUBLIC_URL + '/img/arnold-avatar.jpg'
      },
      chatName: 'My chat',
      editMessageId: null,
      editValue: ''
    }
  }

  getParticipantsNumber = () => {
    let participants = [];

    this.state.messages.forEach(message => {
      if (!participants.includes(message.id)) {
        participants.push(message.id);
      }
    })

    return participants.length;
  }

  onChangeHandler = e => {
    const value = e.target.value;

    this.setState(prevState => ({
      ...prevState,
      textareaValue: value
    }))
  }

  onSendHandler = () => {
    if (!this.state.textareaValue.trim()) return;

    const newMessage = {
      id: uuidv4(),
      text: this.state.textareaValue,
      user: this.state.user.name,
      avatar:  this.state.user.avatar,
      userId: this.state.user.id,
      editedAt: '',
      createdAt: new Date().toString(),
      likes: []
    }

    this.setState(prevState => ({
      ...prevState,
      messages: [...prevState.messages, newMessage],
      textareaValue: ''
    }))
  }

  onDeleteHandler = id => {
    this.setState(prevState => ({
      ...prevState,
      messages: prevState.messages.filter(message => message.id !== id)
    }))
  }

  onEditClick = (id, text) => {
    this.setState(prevState => ({
      ...prevState,
      editMessageId: id,
      editValue: text
    }))
  }

  onEditChange = e => {
    const value = e.target.value;

    this.setState(prevState => ({
      ...prevState,
      editValue: value
    }))
  }

  onEditHandler = () => {
    const message = this.state.messages.find(message => (
      message.id === this.state.editMessageId
    ));
    const messages = this.state.messages.filter(message => (
      message.id !== this.state.editMessageId
    ));

    this.setState(prevState => ({
      ...prevState,
      messages: [
        ...messages,
        {...message, text: prevState.editValue, editedAt: new Date().toString()}
      ],
      editMessageId: null,
      editValue: ''
    }))
  }

  onLikeHandler = id => {
    const messages = [...this.state.messages];
    const message = messages.find(message => (message.id === id));

    if (message.userId === this.state.user.id) return;

    if (message.likes.includes(this.state.user.id)) {
      message.likes = message.likes.filter(userId => userId !== this.state.user.id);
    } else {
      message.likes.push(this.state.user.id);
    }

    this.setState(prevState => ({
      ...prevState,
      messages
    }))
  }

  async componentDidMount() {
    try {
      this.setState(prevState => ({ ...prevState, isLoading: true }))

      const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
      let messages = await response.json();
      messages = messages.map(message => {
        message.likes = [];
        return message
      })

      this.setState(prevState => ({...prevState, messages, isLoading: false}));
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    if (this.state.isLoading) {
      return <div className={classes.Chat}>
        <Loader />
      </div>
    }

    const {
      messages, textareaValue, chatName,
      user, editMessageId, editValue
    } = this.state;
    const participantsNumber = this.getParticipantsNumber();
    const lastMessage = messages[messages.length - 1];

    return (
      <div className={classes.Chat}>
        <ChatHeader
          participantsNumber={participantsNumber}
          messagesNumber={messages.length}
          lastMessage={lastMessage}
          chatName={chatName}
        />
        <MessageList
          messages={messages}
          user={user}
          onDeleteHandler={this.onDeleteHandler}
          editMessageId={editMessageId}
          editValue={editValue}
          onEditClick={this.onEditClick}
          onEditChange={this.onEditChange}
          onEditHandler={this.onEditHandler}
          onLikeHandler={this.onLikeHandler}
        />
        <MessageInput
          value={textareaValue}
          onChangeHandler={this.onChangeHandler}
          onSendHandler={this.onSendHandler}
        />
      </div>
    )
  }
}

