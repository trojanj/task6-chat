import React from 'react';
import PropTypes from 'prop-types';

import classes from './EditMessage.module.css';

export const EditMessage = ({editValue, onEditChange, onEditHandler}) => {
  return (
    <div className={classes.editMessage}>
      <textarea value={editValue} rows={1} onChange={(e) => onEditChange(e)}/>
      <button onClick={() => onEditHandler()}>Save</button>
    </div>
  )
}

EditMessage.propTypes = {
  editValue: PropTypes.string,
  onEditChange: PropTypes.func,
  onEditHandler: PropTypes.func
}
