import React from 'react';
import { getDateTime } from '../../helpers';
import { EditMessage } from './EditMessage/EditMessage';
import PropTypes from 'prop-types';

import classes from './Message.module.css';

export const Message = ({
                  message, userData, onDeleteHandler,
                  editMessageId, onEditClick, onEditChange,
                  editValue, onEditHandler, onLikeHandler
}) => {
  const {text, avatar, user, createdAt, id , likes, userId} = message;
  let time = getDateTime(new Date(createdAt));
  const ind = time.lastIndexOf(':');
  time = time.slice(0, ind);

  const likesClasses = [classes.like];

  if (likes.includes(userData.id)) {
    likesClasses.push(classes.active)
  }

  const cls = [classes.MessageBlock];
  if (userId === userData.id) {
    cls.push(classes.own);
  }

  return (
    <div className={cls.join(' ')}>
      { userData.id !== userId
        && <div className={classes.authorBlock}>
            <div className={classes.img}>
              <img src={avatar} alt="avatar"/>
            </div>
            <div className={classes.username}>{user}</div>
          </div>
      }
      {
        editMessageId !== id
          ? <div className={classes.message}>
              <pre>{text}</pre>
              <div className={classes.toolbar}>
                <i
                  className="fas fa-edit fa-xs"
                  onClick={() => onEditClick(id, text)}
                />
                <i
                  className="fas fa-trash fa-xs"
                  onClick={() => onDeleteHandler(id)}
                />
              </div>
              <div className={classes.time}>{time}</div>
              <div className={likesClasses.join(' ')}>
                <div>{likes.length}</div>
                <i
                  className="fas fa-thumbs-up fa-lg"
                  onClick={() => onLikeHandler(id)}
                />
              </div>
            </div>
          : <EditMessage
              editValue={editValue}
              onEditChange={onEditChange}
              onEditHandler={onEditHandler}
            />
      }
    </div>
  )
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  userData: PropTypes.objectOf(PropTypes.string),
  onDeleteHandler: PropTypes.func,
  editMessageCreatedAt: PropTypes.string,
  editValue: PropTypes.string,
  onEditClick: PropTypes.func,
  onEditChange: PropTypes.func,
  onEditHandler: PropTypes.func,
  onLikeHandler: PropTypes.func
}
