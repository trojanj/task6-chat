import React from 'react';
import { getDateTime, getTextPassedDays, passedDaysFromDate } from '../../helpers';
import PropTypes from 'prop-types';

import classes from './ChatHeader.module.css';

export const ChatHeader = ({ participantsNumber, messagesNumber, lastMessage, chatName }) => {
  const lastMessageDate = new Date(lastMessage?.createdAt);
  const lastMessageTime = getDateTime(lastMessageDate);
  const passedDays = passedDaysFromDate(lastMessageDate);
  const day = getTextPassedDays(passedDays, lastMessageDate);

  return (
    <div className={classes.ChatHeader}>
      <div>
        <span>{chatName}</span>
        <span>
          {
            participantsNumber > 1
              ? `${participantsNumber} participants`
              : `${participantsNumber} participant`
          }
        </span>
        <span>
          {
            messagesNumber > 1
            ? `${messagesNumber} messages`
            : `${messagesNumber} message`
          }
        </span>
      </div>

      <span>last message {day === 'today' ? '' : day} at {lastMessageTime}</span>
    </div>
  )
}

ChatHeader.propTypes = {
  participantsNumber: PropTypes.number,
  messagesNumber: PropTypes.number,
  lastMessage: PropTypes.objectOf(PropTypes.any),
  chatName: PropTypes.string
}
