import React from 'react';
import { getTextPassedDays } from '../../../helpers';
import PropTypes from 'prop-types';

import classes from './Timeline.module.css';

export const Timeline = ({ passedDays, messageDate }) => {
  const day = getTextPassedDays(passedDays, messageDate);

  return (
    <div className={classes.Timeline}>
      <span>{day}</span>
    </div>
  )
}

Timeline.propTypes = {
  passedDays: PropTypes.number,
  messageDate: PropTypes.instanceOf(Date)
}
