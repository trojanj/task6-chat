import React from 'react';

import classes from './Header.module.css';

export default () => {
  return (
    <div className={classes.Header}>
      <div>My chat</div>
    </div>
  )
}
