import React from 'react';
import classes from './MessageInput.module.css';
import PropTypes from 'prop-types';

export const MessageInput = ({value, onChangeHandler, onSendHandler}) => {
  return (
    <div className={classes.MessageInput}>
      <textarea
        placeholder='Write something'
        rows={1}
        value={value}
        onChange={onChangeHandler}
      />
      <button type='submit' onClick={onSendHandler}>Send</button>
    </div>
  )
}

MessageInput.propTypes = {
  value: PropTypes.string,
  onChangeHandler: PropTypes.func,
  onSendHandler: PropTypes.func
}
