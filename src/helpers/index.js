import { MS_IN_ONE_DAY } from '../constants';

export const passedDaysFromDate = date => {
  const currentDate = new Date().setHours(0,0,0,0);
  const copyDate = date.setHours(0,0,0,0);

  return (currentDate - copyDate) / MS_IN_ONE_DAY;
}

export const getTextPassedDays = (passedDays, messageDate) => {
  if (passedDays === 0) {
    return 'today';
  } else if (passedDays === 1) {
    return 'yesterday';
  } else if (passedDays < 8) {
    const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    return days[messageDate.getDay()];
  } else {
    const months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
    return messageDate.getDate() + ' ' + months[messageDate.getMonth()];
  }
}

export const getDateTime = date => {
  const arr = [date?.getHours(), date?.getMinutes(), date?.getSeconds()];
  return arr
    .map(item => item < 10 ? '0' + item : item)
    .join(':')
}