import React from 'react';
import { Chat } from './containers/Chat';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

const App = () => {
  return (
    <>
      <Header />
      <Chat />
      <Footer />
    </>
  );
}

export default App;
